# Bacon_ElectrochimicaActa_455_142380_2023

Contains input files and data used to generate the figures of the article:

On the key role of electrolyte-electrode van der Waals interactions in the simulation of ionic liquids-based supercapacitors

Camille Bacon, Alessandra Serva, Céline Merlet, Patrice Simon, and Mathieu Salanne,
*Electrochimica Acta*, 455, 142380, 2023

https://doi.org/10.1016/j.electacta.2023.142380 ([see here for a preprint](https://chemrxiv.org/engage/chemrxiv/article-details/63a0354aa53ea6785d504e28))

The folder *input_files* contains input files for each system for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The folder *raw_data* contain files with the data used to plot the Figures of the paper 
